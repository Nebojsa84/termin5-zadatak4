$(document).ready(function(){
  
    $("button").click(function(){
        $("#content").empty(); // Obrise sadrzaj iz div elementa

        var num = $("#num").val(); 

        $.get("https://jsonplaceholder.typicode.com/posts", function(data, status){
                       
            var i;
            if(num>25){
                alert("Number must be less then 25!");
                $("#num").val("");
            }
            else
            {
                for (i = 0; i < num; i++) { 
                
                    $("#content").append("<h3>"+data[i].title+"</h3><br/>")
                    $("#content").append("<p>"+data[i].body+"</p><br/><hr/>")
                }

            }
            
            
        });
    });

});